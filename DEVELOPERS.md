# Developers
These are the instructions for developers maintaining this repository.

## Creating a release

1. update the dependencies edit the `versions` file and run the `./update-sub-modules.sh`.
2. `git commit -am "Release x.x.x"`
3. `git push origin main`
4. `git tag vx.x.x`
5. `git push origin vx.x.x`

## Publishing a release
To be on the safe side, this requires you to clone a fresh copy of the repository. Use the `black_list_files.txt` to exclude files. It's a good idea to take a quick look at `./publish.sh` to get an idea of what's going on. 

1. `git clone git@git.isis.vanderbilt.edu:aa-caid/caid-tools.git`
2. `cd caid-tools`
3. `./publish x.x.x`