#!/bin/bash
source versions

function checkout_version() {
  local dir=$1
  local version=$2

  echo "$dir - version $version"

  cd $dir

  git fetch --all
  git checkout "v$version"

  cd ..
}

checkout_version 'depi-impl' $DEPI_IMPL
checkout_version 'webgme-depi' $WEBGME_DEPI
checkout_version 'gsn-domain' $GSN_ASSURANCE

# Check for uncommitted changes
if ! git diff-index --quiet HEAD --; then
  echo "There are updates in the submodules - make sure to commit"
  exit 1
fi